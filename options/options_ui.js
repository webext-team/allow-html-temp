// From top of the HTML options page to bottom to be working correctly!
var parentAndChilds = [];
parentAndChilds = [
  ["appRemoteContent",["tempRemoteContent", "tempRemoteContentLabelId"]],
  ["allwaysInline",["tempInline", "tempInlineLabelId"]]
];

// Enable or disable options UI child elements
function enableOrDisableOptionUiElements() {
  parentAndChilds.forEach((parentAndChild) => {
    consoleDebug("AHT: parentElement: " + parentAndChild[0]);
    // Cave: In this Addon the logic is vice-versa -> when the parent is disabled, the child is enabled!
    if ((!document.getElementById(parentAndChild[0]).checked) || (document.getElementById(parentAndChild[0]).getAttribute("disabled"))) {
      parentAndChild[1].forEach((childElement) => {
        consoleDebug("AHT: enable child: " + childElement);
        if ((document.getElementById(childElement).nodeName == "INPUT") || (document.getElementById(childElement).nodeName == "SELECT")) {
          document.getElementById(childElement).removeAttribute("disabled");
        }
        else {
          document.getElementById(childElement).classList.remove("disabled");
        }
      });
    } else {
      parentAndChild[1].forEach((childElement) => {
        consoleDebug("AHT: disable child: " + childElement);
        if ((document.getElementById(childElement).nodeName == "INPUT") || (document.getElementById(childElement).nodeName == "SELECT")) {
          document.getElementById(childElement).setAttribute("disabled", "true");
        }
        else {
          document.getElementById(childElement).classList.add("disabled");
        }
      });
    }
  });
}