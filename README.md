***Allow HTML Temp***

--------

### Features

* Allows to have HTML temporarily allowed in the currently displayed message by only one click. When switching to another message, it'll be shown automatically again in plain text or sanitized html mode (according to your default mode).

### Version series

* Version 10.*  - Thunderbird 128.* - ESMification
* Version 9.*   - Thunderbird 115.* Supernova - Partially rewritten including conceptual changes to fit for Thunderbird Supernova UI and internal code changes
* Version 8.*   - Thunderbird 102.* - Migrated to a MailExtension using own experiment APIs
* Version 7.*   - Thunderbird 91.*
* Version 6.*   - Thunderbird 78.*
* Version 5.*   - Thunderbird 68.*

### Known issues

* It's not possible to use the provided functions to easily print, forward or answer with HTML temporarily allowed. It seems there is no possibility to implement these repeatedly requested features by an addon. It would be necessary to change Thunderbird core functions, to get these things working. If you are interested in getting the whole functionality build in Thunderbirds core, then contribute to the RFE [Bug 1598857](https://bugzilla.mozilla.org/show_bug.cgi?id=1598857).

### Installation

1. [Download Allow HTML Temp from the official Thunderbird add-on page](https://addons.thunderbird.net/addon/allow-html-temp/)
2. [Installing an Add-on in Thunderbird](https://support.mozilla.org/kb/installing-addon-thunderbird)


### Contribution

You are welcomed to contribute to this project by:
* adding or improving the localizations of AttachmentExtractor Continued via email to me or a post in german Thunderbird forums [Thunderbird Mail DE](https://www.thunderbird-mail.de/forum/board/81-hilfe-und-fehlermeldungen-zu-thunders-add-ons/) or just create an [issue](https://gitlab.com/ThunderbirdMailDE/allow-html-temp/issues/) here on GitLab
* creating [issues](https://gitlab.com/ThunderbirdMailDE/allow-html-temp/issues/) about problems
* creating [issues](https://gitlab.com/ThunderbirdMailDE/allow-html-temp/issues/) about possible improvements


### Coders

* Alexander Ihrig (Original Author and Maintainer)
* John Bieling (Addon developer support)
* Jonathan Kamens (RemoteContent policy API based on his work)

### Translators

* de  - Alexander Ihrig
* dsb - milupo
* en  - Alexander Ihrig
* fr  - DenB10
* hsb - milupo
* ja  - ysdev1


### License

[Mozilla Public License version 2.0](https://gitlab.com/ThunderbirdMailDE/allow-html-temp/LICENSE)