var ahtFunctions = {

  allowHtmlTemp: async function(tabId, infoModifiers, remoteButton) {

    // RemoteContent popupmenu item clicked in remote content bar in a HTML message
    if (remoteButton === true) {
      consoleDebug("AHT: allowHtmlTemp: remoteButton === true");
      ahtFunctions.ShowRemote(tabId);
    }

    // We must now differ the chosen function by modifier key (ahtKeyboardEvent).

    // Addon button clicked + both CTRL and SHIFT key
    else if((infoModifiers.includes("Ctrl") || infoModifiers.includes("Command")) && infoModifiers.includes("Shift")) {
      consoleDebug("AHT: allowHtmlTemp: Addon button clicked + both CTRL and SHIFT key");
      ahtFunctions.ShowSanitizedHTML();
    }

    // Addon button clicked + only CTRL key
    else if((infoModifiers.includes("Ctrl") || infoModifiers.includes("Command")) && !(infoModifiers.includes("Shift"))) {
      consoleDebug("AHT: allowHtmlTemp: Addon button clicked + only CTRL key");
      ahtFunctions.ShowRemote(tabId);
    }

    // Addon button clicked + only SHIFT key
    else if ((infoModifiers.includes("Shift")) && !((infoModifiers.includes("Ctrl") || infoModifiers.includes("Command")))) {
      consoleDebug("AHT: allowHtmlTemp: Addon button clicked + only SHIFT key");
      ahtFunctions.ShowPlaintext();
    }

    // Addon button clicked - no key pressed
    else if (!(infoModifiers.includes("Shift") || infoModifiers.includes("Ctrl") || infoModifiers.includes("Command"))) {
      consoleDebug("AHT: allowHtmlTemp: keyboard command pressed || button clicked + no key pressed");
      consoleDebug("AHT: allowHtmlTemp: options.buttonHtmlMode = " + options.buttonHtmlMode);
      switch (options.buttonHtmlMode) {
        case "buttonMode_html":
          if (options.tempRemoteContent) {
            consoleDebug("AHT: html + tempRemoteContent");
            ahtFunctions.ShowRemote(tabId);
          } else {
            consoleDebug("AHT: html");
            ahtFunctions.ShowOriginalHTML();
          }
          break;
        case "buttonMode_sanitized":
          consoleDebug("AHT: sanitized");
          ahtFunctions.ShowSanitizedHTML();
          break;
        case "buttonMode_plaintext":
          consoleDebug("AHT: plaintext");
          ahtFunctions.ShowPlaintext();
          break;
        default:
          consoleDebug("AHT: default");
      }
    }
  },

  ShowPlaintext: async function() {
    consoleDebug("AHT: ShowPlaintext");
    try {
      let updateProperties = {
        msgBodyAs: "plaintext"
      };
      await messenger.messageContentPolicy.update(updateProperties);
    } catch (e) {
      console.error("AHT: Plaintext error");
    }
  },

  ShowSanitizedHTML: async function() {
    consoleDebug("AHT: ShowSanitizedHTML");
    try {
      let updateProperties = {
        msgBodyAs: "sanitized"
      };
      await messenger.messageContentPolicy.update(updateProperties);
    } catch (e) {
      console.error("AHT: ShowSanitizedHTML error");
    }
  },

  ShowOriginalHTML: async function() {
    consoleDebug("AHT: ShowOriginalHTML");
    try {
      let updateProperties = {
        msgBodyAs: "original",
        // do not use false for attachmentsInline, but null to leave option unset if not tempInline
        attachmentsInline: ((options.tempInline === true) ? true : null)
      };
      await messenger.messageContentPolicy.update(updateProperties);
    } catch (e) {
      console.error("AHT: ShowOriginalHTML error");
    }
  },

  ShowRemote: async function(tabId) {
    consoleDebug("AHT: ShowRemote");
    try {
      let message = await messenger.messageDisplay.getDisplayedMessage(tabId);
      consoleDebug("AHT: ShowRemote: message.id = ", message.id);
      await messenger.remoteContent.setContentPolicy(message.id, "Allow");
    } catch (e) {
      console.error("AHT: ShowRemote ERROR with remoteContent policy");
    }
    try {
      let updateProperties = {
        msgBodyAs: "original",
        disableRemoteContent: false,
        // do not use false for attachmentsInline, but null to leave option unset if not tempInline
        attachmentsInline: ((options.tempInline === true) ? true : null)
      };
      await messenger.messageContentPolicy.update(updateProperties);
    } catch (e) {
      console.error("AHT: ShowRemote ERROR with messageContent policy");
    }
  }
};