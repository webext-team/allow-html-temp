// Enable if action buttons
// - if message is not Junk
// - if message is not News
// - if message has an HTML MIME part
async function updateActionButtonForNewMessages(tab, messages) {
  // messages is technically an array of objects, which in theory should only 
  // have one object at this point. Nevertheless we need to recursively handle this array.
  for (let i = 0; i < messages.length; i++){ 
    consoleDebug("AHT: updateActionButtonForNewMessages: messages Array i = " + i);

    let message = messages[i];

    consoleDebug("AHT: updateActionButtonForNewMessage: tab.id = " + tab.id + " message.id = " + message.id);

    // check for folder type and don't enable UI for junk folders
    consoleDebug("AHT: updateActionButtonForNewMessage: folder type = " + message.folder.type);
    if (message.folder.type == "junk") {
      messenger.messageDisplayAction.disable(tab.id);
      return;
    }
    // check for junk status and don't enable UI for junk messages
    consoleDebug("AHT: updateActionButtonForNewMessage: junk status = " + message.junk);
    if (message.junk) {
      messenger.messageDisplayAction.disable(tab.id);
      return;
    }
  
    // check for account type:
    // don't enable UI for News accounts,
    // but enable UI for RSS accounts
    let account = await messenger.accounts.get(message.folder.accountId);
    consoleDebug("AHT: updateActionButtonForNewMessage: account type = " + account.type);
    if (account.type == "news") {
      messenger.messageDisplayAction.disable(tab.id);
      return;
    }
    
    if (account.type == "rss") {
      consoleDebug("AHT: updateActionButtonForNewMessage: enable toolbar button");
      messenger.messageDisplayAction.enable(tab.id); // Unconditionally for all rss messages?
      return;
    }
  
    // for all other messages check, if they have a HTML MIME part
    consoleDebug("AHT: updateActionButtonForNewMessage: checkMailForHtmlpart");
    let hasHtmlMimePart = await messenger.allowHtmlTemp.checkMailForHtmlpart(message.id, options.debug);
    consoleDebug("AHT: updateActionButtonForNewMessage: checkMailForHtmlpart returns: " + hasHtmlMimePart);
  
    if (hasHtmlMimePart) {
      messenger.messageDisplayAction.enable(tab.id);
    } else {
      messenger.messageDisplayAction.disable(tab.id);
    }
  }
};

async function setButtonIconAndLabel() {
  buttonIcon = {};
  buttonLabel = {};

  await reloadOption("buttonHtmlMode");
  let appRemoteContent = !(await messenger.messageContentPolicy.getCurrent()).disableRemoteContent;
  await reloadOption("tempRemoteContent");
  consoleDebug("AHT: setButtonIcon: options.buttonHtmlMode: " + options.buttonHtmlMode);
  consoleDebug("AHT: setButtonIcon: pref appRemoteContent: " + appRemoteContent);
  consoleDebug("AHT: setButtonIcon: options.tempRemoteContent: " + options.tempRemoteContent);

  switch (options.buttonHtmlMode) {
    case "buttonMode_html":
      if ((appRemoteContent == true) || (options.tempRemoteContent == true)) {
        consoleDebug("AHT: setButtonIcon: html+");
        buttonIcon.path = "../icons/aht_button_supernova_color_plus.svg";
        buttonLabel.label = messenger.i18n.getMessage("button_label_html");
        break;
      } else {
        consoleDebug("AHT: setButtonIcon: html");
        buttonIcon.path = "../icons/aht_button_supernova_color.svg";
        buttonLabel.label = messenger.i18n.getMessage("button_label_html");
        break;
      }
    case "buttonMode_sanitized":
      consoleDebug("AHT: setButtonIcon: sanitized");
      buttonIcon.path = "../icons/aht_button_supernova_sanitized_lightdarkcss.svg";
      buttonLabel.label = messenger.i18n.getMessage("button_label_sanitized");
      break;
    case "buttonMode_plaintext":
      consoleDebug("AHT: setButtonIcon: plaintext");
      buttonIcon.path = "../icons/aht_button_supernova_plaintext_lightdarkcss.svg";
      buttonLabel.label = messenger.i18n.getMessage("button_label_plaintext");
      break;
    default:
      consoleDebug("AHT: setButtonIcon: default");
      buttonIcon.path = "../icons/aht_button_supernova_color.svg";
      buttonLabel.label = messenger.i18n.getMessage("button_label_html");
      break;
  }

  messenger.messageDisplayAction.setIcon(buttonIcon);
  messenger.messageDisplayAction.setLabel(buttonLabel);
}

async function setCommandKey() {
  await reloadOption("commandKey");
  consoleDebug("AHT: setCommandKey: options.commandKey: " + options.commandKey);

  let detail = {};
  detail.name = "_execute_message_display_action";
  detail.shortcut = options.commandKey;
  if (detail.shortcut === "") {
    consoleDebug("AHT: setCommandKey: Your chosen commandkey is empty. Therefore the default \"" + DefaultOptions.commandKey + "\" will be used.");
    detail.shortcut = DefaultOptions.commandKey;
  }
  try {
    await messenger.commands.update(detail);
  } catch (e) {
    consoleDebug("AHT: setCommandKey: Your chosen commandkey isn't valid. Therefore the default \"" + DefaultOptions.commandKey + "\" will be used.");
    detail.shortcut = DefaultOptions.commandKey;
    await messenger.commands.update(detail);
    // Reset option to default
    return messenger.storage.local.remove("commandKey").then(() => {
    });
  }
}

setButtonIconAndLabel();
setCommandKey();
