const DefaultOptions = {
  debug: false,
  commandKey: "Ctrl+Alt+F5",
  buttonHtmlMode: "buttonMode_html",
  tempRemoteContent: false,
  tempInline: false
}
const OptionsList = Object.keys(DefaultOptions);

const DefaultPrefs = {
  appHtmlMode: "appMode_sanitized",
  appRemoteContent: true,
  allwaysInline: false
}
const PrefsList = Object.keys(DefaultPrefs);

function defaultError(error) {
  console.error("AHT: Error:", error);
}