var options = {};

async function optionsMigrate() {
  // Because of old LegacyPrefs using other value then the new options system, we can't do a simple recursion for the 3 LegacyPrefs.
  // Therefore the following procedures (only) seem to be redundant.

  let legacyName = "";
  let legacyValue = null;

  legacyName = "extensions.allowHtmlTemp.ButtonFunction";
  legacyValue = null;
  legacyValue = await messenger.LegacyPrefs.getUserPref(legacyName);

  if (legacyValue !== null) {
    console.log("Migrating legacy preference: " + legacyName + " = ", legacyValue);

    // Migrate old integer value to new string value
    // Sometimes, there is a strange issue leading to string vs integer values. Get both in the switch:
    switch (legacyValue) {
      case 0:
        newValue = "buttonMode_html";
        break;
      case "0":
        newValue = "buttonMode_html";
        break;
      case 1:
        newValue = "buttonMode_sanitized";
        break;
      case "1":
        newValue = "buttonMode_sanitized";
        break;
      case 2:
        newValue = "buttonMode_plaintext";
        break;
      case "2":
        newValue = "buttonMode_plaintext";
        break;
    }
    messenger.storage.local.set({
      buttonHtmlMode: newValue
    });

    // Clear the legacy value.
    messenger.LegacyPrefs.clearUserPref(legacyName);
  }

  legacyName = "extensions.allowHtmlTemp.ForceRemoteContent";
  legacyValue = null;
  legacyValue = await messenger.LegacyPrefs.getUserPref(legacyName);
  if (legacyValue !== null) {
    console.log("Migrating legacy preference: " + legacyName + " = ", legacyValue);

    messenger.storage.local.set({
      tempRemoteContent: legacyValue
    });

    // Clear the legacy value.
    messenger.LegacyPrefs.clearUserPref(legacyName);
  }

  legacyName = "extensions.allowHtmlTemp.InlineAttachmentsTemp";
  legacyValue = null;
  legacyValue = await messenger.LegacyPrefs.getUserPref(legacyName);
  if (legacyValue !== null) {
    console.log("Migrating legacy preference: " + legacyName + " = ", legacyValue);

    messenger.storage.local.set({
      tempInline: legacyValue
    });

    // Clear the legacy value.
    messenger.LegacyPrefs.clearUserPref(legacyName);
  }

  // Clear 2 more old legacy prefs
  messenger.LegacyPrefs.clearUserPref("extensions.allowHtmlTemp.JavaScriptTemp");
  messenger.LegacyPrefs.clearUserPref("extensions.allowHtmlTemp.reset_javascript_default_done_once");
}


async function optionsInit() {
  await reloadAllOptions();
}

async function reloadAllOptions() {
  await reloadOption("debug");
  await reloadOption("commandKey");
  await reloadOption("buttonHtmlMode");
  await reloadOption("tempRemoteContent");
  await reloadOption("tempInline");
}

function reloadOption(id) {
  return messenger.storage.local.get(id).then((res) => {
    if (res[id] != undefined)
      options[id] = res[id];
    else
      options[id] = DefaultOptions[id];
  }, defaultError);
}

async function getOption(name) {
  if (!DefaultOptions[name]) {
    throw new Error(`Unknown option ${name}`)
  }
  let rv = await messenger.storage.local.get({[name]: DefaultOptions[name]});
  return rv[name];
}
