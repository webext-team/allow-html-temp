allow-html-temp (10.0.7-1) unstable; urgency=medium

  [ Mechtilde ]
  * [acc0078] New upstream version 10.0.7
  * [1eecb0d] Bumped standard version - no changes needed
  * [1812b91] Bumped usable version of thunderbird

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sat, 08 Mar 2025 15:02:56 +0100

allow-html-temp (10.0.6-1) unstable; urgency=medium

  [ Mechtilde ]
  * [1e0a456] New upstream version 10.0.6
  * [136335b] Bumped max. version for thunderbird

 -- Mechtilde Stehmann <mechtilde@debian.org>  Fri, 27 Dec 2024 16:08:27 +0100

allow-html-temp (10.0.5-1) unstable; urgency=medium

  [ Mechtilde ]
  * [80b13e6] New upstream version 10.0.5

 -- Mechtilde Stehmann <mechtilde@debian.org>  Wed, 18 Sep 2024 17:04:07 +0200

allow-html-temp (10.0.4-1) unstable; urgency=medium

  [ Mechtilde ]
  * [9600c16] New upstream version 10.0.4
  * [6fbc1c4] Added Link to a.t.n
  * [dfdd43f] Bumped year of copyright
  * [f18e5c4] Bumped standard version - no changes needed
  * [50fede0] Bumped to recent thunderbird esr version
  * [1bf6b7b] Improved d/watch
  * [c6b36d3] Fixed version number in d/control

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sat, 31 Aug 2024 11:39:57 +0200

allow-html-temp (9.0.0-2) unstable; urgency=medium

  [ Mechtilde ]
  * [9461d49] Fixed d/watch

 -- Mechtilde Stehmann <mechtilde@debian.org>  Wed, 15 May 2024 19:21:45 +0200

allow-html-temp (9.0.0-1) unstable; urgency=medium

  [ Mechtilde ]
  * [f6b9b32] New upstream version 9.0.0
  * [187f994] Bumped thunderbird version to 115 and limit it to 119
  * [2662398] Bumped standard version to recent one - no changes needed
  * [82471ba] Fixed max version of thunderbird

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sun, 03 Sep 2023 12:58:30 +0200

allow-html-temp (8.1.5-1) unstable; urgency=medium

  [ Mechtilde ]
  * [e0ead76] New upstream version 8.1.5

 -- Mechtilde Stehmann <mechtilde@debian.org>  Wed, 16 Nov 2022 19:04:54 +0100

allow-html-temp (8.1.3-1) unstable; urgency=medium

  [ Mechtilde ]
  * [847ac76] New upstream version 8.1.3

 -- Mechtilde Stehmann <mechtilde@debian.org>  Fri, 14 Oct 2022 14:37:07 +0200

allow-html-temp (8.1.2-2) unstable; urgency=medium

  [ Mechtilde ]
  * [72db1b6] Added new files and directories from upstream to d/rules

 -- Mechtilde Stehmann <mechtilde@debian.org>  Fri, 30 Sep 2022 20:30:53 +0200

allow-html-temp (8.1.2-1) unstable; urgency=medium

  [ Mechtilde ]
  * [e929d7f] New upstream version 8.1.2
  * [09fcfdb] Changed min version of thunderbird

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sun, 07 Aug 2022 12:58:34 +0200

allow-html-temp (8.0.3-1) unstable; urgency=medium

  [ Mechtilde ]
  * [c02bf1b] New upstream version 8.0.0
  * [1312d19] Bumped standard version - no changes needed
  * [8a5ce39] Removed entry for unused file
  * [028f532] New upstream version 8.0.3

 -- Mechtilde Stehmann <mechtilde@debian.org>  Thu, 26 May 2022 17:30:54 +0200

allow-html-temp (7.0.2-1) unstable; urgency=medium

  [ Mechtilde ]
  * [36610d7] New upstream version 7.0.2
  * [0c85f93] bump year in d/copyright and added copyright for d/*

 -- Mechtilde Stehmann <mechtilde@debian.org>  Fri, 15 Apr 2022 21:18:50 +0200

allow-html-temp (6.3.7-1) unstable; urgency=medium

  [ Mechtilde ]
  * [a6aadac] New upstream version 6.3.7
  * [bf98f58] Removed URL from d/u/metadata
  * [b3038c0] Bumped standard version -  no changes needed
  * [e79dd09] Bumped to recent version of thunderbird
  * [5c20f88] Reduced length of lines in description in d/control

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sat, 15 Jan 2022 19:53:02 +0100

allow-html-temp (6.3.3-1~exp1) experimental; urgency=medium

  [ Mechtilde ]
  * [c447af3] New upstream version 6.3.3 (Closes: #988594)
  * [9f97b20] Shortened lines in description in d/control

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sun, 16 May 2021 16:56:31 +0200
